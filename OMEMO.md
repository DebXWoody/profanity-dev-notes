# OMEMO Support in profanity

Erst einmal stellt sich mir die Frage, in wie weit man noch Funktionen in die
aktuelle Implementierung einbaut oder ggf. auf ein Folgerelease verschiebt.

Grund: Mit der Version 0.4.0 von [XEP-0384: OMEMO Encryption](https://xmpp.org/extensions/xep-0384.html) 
hat sich unter unter anderem auch der namespace der XEP geändert. D.h. es
müssten dann beide Version unterstützt werden, was ggf. einige Anpassungen im
aktuellen Code bedeutet.

In der Profanity 0.10.x ist aktuell die Version Version 0.3.0 (2018-07-31) von
XEP-0384 implementiert. Dies wir auch sicherlich für 0.11.x der Fall sein.

## Status 0.10.x

In der Version profanity gibt es einige Fehler/Probleme welche in der Version zu
0.11.x und später behoben werden sollten bzw. auch schon wurden.

### Access model

Der Wert von `access model` sollte `open` sein. Dies wurde auch in
Profanity implementiert. Bei der Erstellung des PubSub node wird
gewährleistet, dass der access auf `open` steht. Ist jedoch ein node mit einem
anderen access model schon vorhanden, wurde das Device nicht in die Liste
eingetragen und auch kein Fehler angezeigt.

Mit 0.11.x wird eine Fehlermeldung angezeigt, dass die Device Liste nicht
aktualisiert werden konnte ("Unable to publish own OMEMO device list").

Eine Korrektur von einem anderen access model auf open sehe ich für 0.11 ggf.
nicht.

# Use cases

* [Login](#login)
* [Kontaktperson erzeugt ein Key auf einem neuen Device](#kontaktperson-erzeugt-ein-key-auf-einem-neuen-device)

## Login

Was passiert nach dem login? Es sollte noch mal genauer die Funktionalität nach
dem login analysiert werden.

Ablauf (grob anhand logging):

* [OMEMO] Loading OMEMO identity
* Received Publish-Subscribee headline event ( alle und nicht nur online user?)
* publish own OMEMO bundle
* publish crypto materials
* [OMEMO] Request OMEMO Bundles for our devices
* request omemo bundle für alle unsere device ids
* Aufbau einer Session mit all unseren devices (`omemo_start_device_session_handle_bundle`)

Hinweis: Die eingehenden headline events können sogar vor dem Empfang des Roster
kommen.

Es wurden auch für alle Kontakte im Adressbuch nach dem login ein OMEMO Session
start durchgeführt. Das führte dann jedoch zu Problemen (Lost connection wegen
timeout). Bei einem Session start werden auch die Keys ausgetauscht. Dies wird
in den meisten Fällen vielleicht direkt nach dem Start für alle Kontakte unnötig
sein.

### Loading OMEMO identity
Als erstes wird die eigene OMEMO Identität geladen. Wenn dies erfolgt ist, wird
der Status *OMEMO loaded* gesetzt - vorher ist OMEMO nicht aktiv.
Wir sollten prüfen, ob dies schon vor dem login selber erfolgt oder erst nach
dem login. Nach login kommen sehr schnell schon die `headline` events.

### Received Publish-Subscribe headline event
Nachdem sich der Account angemeldet hat, werden ohne explizites Anfragen die
PubSub events vom Server an den Client geschickt. Hier bekommen wir die Device
IDs von unseren Kontakten mitgeteilt.

## Kontaktperson erzeugt ein Key auf einem neuen Device

Sowohl via PubSub `headline` `event` also auch bei der expliziten Anfrage der
Device Liste von Kontaktpersonen, kann ggf. ein neues Device gefunden werden.

Zunächst ist es nötig, dass man die *bekannten* devices und die *neuen* klar
identifizieren kann. D.h. *Wann ist ein Device bekannt und wann ist es neu?*.

Aktuelle Implementierung (aber noch mal zu prüfen)

 * Device Liste wird explizit oder implizit via `event` gesetzt
 * Wenn eine OMEMO Session gestartet wird, werden auch die OMEMO bundle
   abgefragt
 * Die bundle Informationen beinhalten auch den Key und ermöglichen so dem
   Device einem Fingerprint zuzuordnen
 * Die Information werden dann ein einer Datei `known_devices.txt` abgelegt

Problem: Die *bekannten* Devices sind erst *bekannt* wenn man eine Session
gestartet hat. Das war auch früher bei einem "connect" der Fall, haben wir aber
ausgebaut, es gab zu viele Problem beim start (Lost connection).

# Implementation notes



